var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
  title: String,    //Provided by cURL  
  link: String,     //Provided by cURL
  upvotes: { 
    type: Number,
    default: 0
  },
  //as it suggests, it is an array of comments i.e comments: [...]
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}]
});

//I don't understand how this function works, but it seems the function is called by a Post object and the method upvote increments that object's upvote attribute. While, in the handler it has own callback function which I don't know how its allowed - but allows to send a response back to cURL.
//Read the API for instance methods, turns out you can use a callback that has the returned data.
PostSchema.methods.upvote = function(cb) {
  this.upvotes += 1;
  this.save(cb);
};

mongoose.model('Post', PostSchema);
