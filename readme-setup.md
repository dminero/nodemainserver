NOTICE: SERVER is still under development, Routes have not been clearly been defined and handling for routes. This is still a TEST SERVER.

This server requires the following software installed on the host computer.
node (v4.4 or Greater) Link? --> https://nodejs.org/en/
MongoDB (v3.2 or Greater) Link? -->https://www.mongodb.com/download-center#community

## Starting the server ##

Before starting the server, you will need to start the database first. Go the directory of the database (where you will store your data, not the program directory - Hence create a folder location to store the data) and ./data/db, open a command-line there and type the command
`mongod --dbpath=.`

The database is up if you see:

[initandlisten] waiting for connections on port 27017

To start the server issue this command:
`npm start`
Which runs 'node .\bin\www'

If the server has successfully run then you should be able to interact with it without error in the command-line or web browser.

## Interacting with the server ##

This server is can to respond to both User interfacing and Command-line interfacing via cURL or curl command.

### Web Browser (User) Interface ###

Server is to handle the traffic and any kind of back end service that is required from the front-end development team.
This is not part of server development domain - whatever front-end work is done, back-end will try to accomplish or accommodate - it dosen't have say other than 'Can we do it or not?'

What is on the front end is a sample from the tutorial that was used to develop this. Front-end **Need to replace the current angular and HTML pages with their work** which I will document how.

### Command-Line Interfacing ###

Windows does not come with curl, you would need to use the Windows PowerShell and Invoke-WebRequest command.
However, you can give Windows curl if you install Git bash on your computer. I prefer the latter.

Some examples of sending web requests to the server using 

//Send a JSON POST request like this
$ curl --data 'title=soemthing&&link=http://www.abc.com.au' http://localhost:3000/post

//Send a request for all data in /post like this.
$ curl http://localhost:3000/post
