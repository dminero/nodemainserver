## Anatomy of Node Project

Explaination of the root directory of an express project. As written in this [tutorial]()

* `app.js` - This file is the launching point for our app. We use it to import all other server files including modules, configure routes, open database connections, and just about anything else we can think of.

* `bin/` - This directory is used to contain useful executable scripts. By default it contains one called www. A quick peak inside reveals that this script actually includes app.js and when invoked, starts our Node.js server.
    * It WOULD be ideal to have other backend processing scripts that we can call. 

node_modules - This directory is home to all external modules used in the project. As mentioned earlier, these modules are usually installed using npm install. You will most likely not have to touch anything here.

* `package.json` - This file defines a JSON object that contains various properties of our project including things such as name and version number. It can also defines what versions of Node are required and what modules our project depends on. A list of possible options can be found in npm's documentation.
    * Project properties.

* `public/` - As the name alludes to, anything in this folder will be made publicly available by the server. This is where we're going to store JavaScript, CSS, images, and templates we want the client to use.

* `routes/` - This directory houses our Node controllers and is usually where most of the backend code will be stored.
    * Sound interesting...

* `views/` - As the name says, we will put our views here. Because we specified the --ejs flag when initializing our project, views will have the .ejs extension as opposed to the '.jade' extension Jade views use. Although views are ultimately HTML, they are slightly different than any HTML file we might specify in the public/ directory. Views are capable of being rendered directly by Node using the render() function and can contain logic that allows the server to dynamically change the content. Because we are using Angular to create a dynamic experience, we won't be using this feature.
    * Goto view/ tab if --efs has worked if the files are of .ejs
    * For more on ejs, visit here [here](http://www.embeddedjs.com/) as stated it is an 'open-source Template Library', for client side javascript. Give [jade](http://jade-lang.com/) a visit as well as both are achieve same idea

In addition to the above files structure, we are going to add one more folder.
Create a new folder called 'models':

* `models/` This folder will contain our Mongoose schema definitions.

## Starting the Program

Use `npm start` (see package.json)